# GITWorkshop

## Configuration
- git config
  -  --global core.editor "vim"
  -  --global --list
  -  --list

## Information
- git
  - status
  - show
  - log --stat origin/dev..HEAD

## Local repository
- create branch
  - git push --set-upstream origin hotfix-001 (branch created in central repository)
  
## Central repository
- create branch
  - git pull (local available)
  - git checkout <branch> (switch to branch)
